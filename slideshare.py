# -*- coding: utf-8 -*-
import sha
import time
import requests

BASE_URL = 'https://www.slideshare.net/api/2/'
api_key = 'fFHiSlA5'
shared_secret = 'DZDMKco3'


def create_required_prop():
    timestamp = str(int(time.time()))
    hashed = sha.new(shared_secret + timestamp).hexdigest()
    return {'api_key': api_key, 'hash': hashed, 'ts': timestamp}

def get_request_by_props(request, props):
    url = BASE_URL + request
    properties = dict(create_required_prop().items() + props.items())
    return requests.get(url, params=properties).text

def get_request_by_user(request, username):
    return get_request_by_props(request, {'username_for': username})

def get_user_favorites(username):
    return get_request_by_user('get_user_favorites', username)

def get_user_contacts(username):
    return get_request_by_props('get_user_contacts', {'username_for': username, 'detailed': 1, 'limit': 1})

def get_slideshows_by_user(username, limit=100, offset=0):
    return get_request_by_props('get_slideshows_by_user', {'username_for': username, 'detailed': 1, 'limit': limit, 'offset':offset})

def get_slideshows_by_tag(tag, limit=100, offset=0):
    return get_request_by_props('get_slideshows_by_tag', {'tag': tag, 'detailed': 1, 'limit': limit, 'offset':offset})

def get_slideshow_by_id(slideshow_id):
    return get_request_by_props('get_slideshow', {'slideshow_id': slideshow_id, 'detailed': 1})
