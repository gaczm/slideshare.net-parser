from datetime import datetime
from database import Slideshow, Tag, get_session, Follower, Favourite, Comment, RelatedSlideshow, SlideshowTag, Contact
import xmltodict
from sqlalchemy.sql.sqltypes import DateTime
     
slideshow_mapping = {'slideshow_id' : 'ID', 'title':'Title', 'description':'Description', 'owner_login':'Username',
                        'url':'URL', 'creation_date':'Created', 'modification_date':'Updated', 'language':'Language',
                        'format_name':'Format', 'slideshow_type_id':'SlideshowType', 'number_of_downloads':'NumDownloads',
                        'number_of_views':'NumViews', 'number_of_slides':'NumSlides'}

class ExtractedSlideshow:
    def __init__(self, slideshows=[], slideshowTags=[], relates=[], tags=[]):
        self.slideshows = slideshows
        self.slideshowTags = slideshowTags
        self.relates = relates
        self.tags = tags

def extract_tags_from_slideshows_tag(slideshowTags):
    print '// extracting Tag'
    tags = [Tag(tag_name=tag.tag_name) for tag in slideshowTags]   
    return list(set(tags))
 
def extract_slideshow_tags(id, tags):
    print '// extracting SlideshowTag'
    if tags is None:
        return []
    tag = tags['Tag']
    if isinstance(tag, list) == False:
        return [SlideshowTag(tag_name=tag['#text'])]
    return [SlideshowTag(tag_name=t['#text'], slideshow_id=id) for t in tag]

def extract_related_slideshows(related_slideshow_id, relates):
    print '// extracting RelatedSlideshow'
    if relates is None:
        return []
    rel = relates['RelatedSlideshowID']
    if isinstance(rel, list) == False:
        return [RelatedSlideshow(related_slideshow_id=related_slideshow_id, relating_slideshow_id=rel['#text'])]
    return [RelatedSlideshow(related_slideshow_id=related_slideshow_id, relating_slideshow_id=slide['#text']) for slide in rel]

def create_slideshow(slide):
    slideshow = Slideshow()
    for key in slideshow_mapping.keys():
        mappedKey = slideshow_mapping[key]
        setattr(slideshow, key, slide[mappedKey])
    return slideshow

def extract_multiple_slideshows(sld_xml):
    print '// extracting multiple slideshows'
    slides = []
    slideshowTags = []
    related = []
    for slide in sld_xml:
            slideshow = create_slideshow(slide);
            slideshowTags = slideshowTags + extract_slideshow_tags(slideshow.slideshow_id, slide['Tags'])
            related = related + extract_related_slideshows(slideshow.slideshow_id, slide['RelatedSlideshows'])
            slides.append(slideshow)
    tags = extract_tags_from_slideshows_tag(slideshowTags)
    return ExtractedSlideshow(slideshows=slides, slideshowTags=slideshowTags, relates=related, tags=tags)


def extract_single_slideshow(slidshow_xml):
    print '// extracting single slideshow'
    slides = []
    slideshowTags = []
    related = []
    slideshow = create_slideshow(slidshow_xml);
    slideshowTags = slideshowTags + extract_slideshow_tags(slideshow.slideshow_id, slidshow_xml['Tags'])
    related = related + extract_related_slideshows(slideshow.slideshow_id, slidshow_xml['RelatedSlideshows'])
    slides.append(slideshow)
    tags = extract_tags_from_slideshows_tag(slideshowTags)
    return ExtractedSlideshow(slideshows=slides, slideshowTags=slideshowTags, relates=related, tags=tags)

def extract_slideshows_tags_related(sld_xml):  
    if isinstance(sld_xml, list):
       return extract_multiple_slideshows(sld_xml);
    return extract_single_slideshow(sld_xml)

def extract_comments_from_page(slideshow_id, page):
    print '// extracting comments for ' + slideshow_id
    return [Comment(content=com.body, creation_date=com.approximate_creation_date, user_login=com.user_login, slideshow_id=slideshow_id) for com in page.comments]

def extract_likes_from_page(slideshow_id, page):
    print '// extracting likes for ' + slideshow_id
    return [Favourite(slideshow_id=slideshow_id, user_login=user) for user in page.likes]

def extract_followers_for(username, followers):
    print '// extracting followers for ' + username
    return [Follower(followed_user_login=username, following_user_login=user) for user in followers]

def extract_followings_for(username, followings):
    print '// extracting followings for ' + username
    return [Follower(followed_user_login=user, following_user_login=username) for user in followings]

def extract_multiple_favourites(username, fav_xml):
    return  [Favourite(user_login = username, slideshow_id=fav['slideshow_id']) for fav in fav_xml]
                       
def extract_favourites(username, fav_xml):
    print '// extracting favourites for ' + username
    if isinstance(fav_xml, list):
        return extract_multiple_favourites(username, fav_xml)
    return [Favourite(user_login = username, slideshow_id=fav_xml['slideshow_id'])]

def extract_multiple_contacts(username, contacts_xml):
    return [Contact(contacting_user_login = username, contacting_with_user_login=cont['Username']) for cont in contacts_xml]

def extract_contacts(username, contacts_xml):
    print '// extracting contacts for ' + username
    if isinstance(contacts_xml, list):
        return extract_multiple_contacts(username, contacts_xml)
    return [Contact(contacting_user_login = username, contacting_with_user_login=contacts_xml['Username'])]

    