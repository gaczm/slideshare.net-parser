# README #

## Instalacja
**Najlepiej całość odpalać pod virtualenv-em**

Żeby użyć skryptu trzeba zainstalować zależności:

```
$ pip install -r requirements.txt
```

## Użycie

Aby zapobiec zablokowaniu crawlera, do pobierania danych wykorzystujemy obiekt klasy ``UrlDownloader`` limitujący liczbę pobrań w jednostce czasu. Obiekt przekazujemy jako parametr do metod crawlera.

### Parsowanie strony z prezentacja
Dla testów można uruchomić z terminala:

```
$ python parser.py p http://www.slideshare.net/ValaAfshar/how-googleglass-will-change-healthcare
// downloading page
// extracting presentation id
// extracting category
// extracting transcript
// downloading comments
// downloading likes
// offset 0
// offset 20
// offset 40
// offset 60
// offset 80
// offset 100
// offset 120
// number of likes: 116
{
  "category": "Devices & Hardware",
  "comments": [
    {
      "approximate_creation_date": "17-10-2014 15:28",
      "body": "I thought you might get a kick out of this from CNNhealth ;) Man treated for Google Glass addiction!  I was at a loss between chuckling and feeling sad for the OCD behavior:  http://www.cnn.com/2014/10/15/health/google-glass-addiction/index.html?",
      "created_at": "1 day",
      "fetch_date": "18-10-2014 15:28",
      "user_login": "aehannan"
    }
  ],
  "likes": [
    "LiamAtkins2",
    "bachasahib3",
(...)
    "aehannan",
    "amikaja"
  ],
  "transcript": [
    "1. 15WAYS \nGOOGLE GLASS \nWILL TRANSFORM \nHEALTHCARE \n@ValaAfshar",
    "2. Virtual Dictation \nAugmedix allows doctors to reclaim \nvaluable time and energy, so they \ncan focus on what matters most \u2013 \ncaring for patients.",
(...)
    "16. Compliance \nWith HIPAA-compliant app \ndevelopers like Pristine and \nCrowdOptics streaming \naudio and video across the \nhospital network through \nGlass, compliance will not \nbe an issue.",
    "17. www.extremenetworks.com/healthcare/ \nSpecial Thanks To: @bobzemke, @Brian_Clev, @carey_mercier"
  ]
}
```

Natomiast z poziomu innego skryptu:

```
#!python

from parse import fetch_slideshare_info
from downloader import UrlDownloader

downloader = UrlDownloader(600) # request every 10 seconds
slideshare_info = fetch_slideshare_info(downloader, 'http://www.slideshare.net/ValaAfshar/how-googleglass-will-change-healthcare')

print 'Category: %s' % slideshare_info.category
print 'No. comments: %d' % len(slideshare_info.comments)
print 'No. likes: %d' % len(slideshare_info.likes)
```

Po uruchomieniu dostaniemy:

```
Category: Devices & Hardware
No. comments: 1
No. likes: 116
```

### Parsowanie profilu

Analogicznie jak w przypadku prezentacji, możemy przetestować parsowanie z konsoli:

```
$ python parse.py u http://www.slideshare.net/TomHarris9
// fetching followers
// page 1
// fetching followings
// page 1
// page 2
// page 3
// page 4
{
  "city": "Greater Minneapolis-St. Paul Area",
  "country": "United States",
  "description": "While in Canada;\n- Committee chair for the development of CGSB/CCMC standards which resulted in the first time inclusion of closed cell SPF in the 1990 National Building Code.\n- Introduced and developed the use of ccSPF as an Air Barrier material and system leading to new standards and a new market for the Canadian industry.\n- Developed the first formulations for and created the marketing strategy surrounding Walltite.\n- Drove BASF Canada to become the share leader in the Canadian ccSPF market.\n\n\nWhile at BASF - US\n- Developed the first 0-ODP spray foam for the marine industry\n- Developed the Zone3 Technologies marketing concept\n- Lead the intigration of 5 acquisitions as a launch strate...",
  "followers": [
    "Jacks_Smith",
    "DJWilliams754"
  ],
  "followings": [
    "monokote",
    "Steamroller24",
    "vresto1",
    "EGAVAS",
    "jlapotaire",
    "dbazzetta",
    "thebauhub",
    "moxiegarrett",
    "KapilShirurkar",
    "labadim",
    "foxp",
    "joelwmay",
    "krm34243",
    "DJWilliams754",
    "B77E",
    "wilso411",
    "WalterLiewald",
    "deanhorowitz",
    "TomGiancani",
    "MarkWhiting1",
    "RafaelMier",
    "KAMBIZTAHERI",
    "jameskirby47",
    "robertquesnette",
    "scottvankerkhove",
    "AngelaSchmitz",
    "AaronCraig",
    "ToddWishneski",
    "EugeneZimmermann"
  ],
  "full_name": "Tom Harris",
  "joined": "2014-02-25 23:18:12 UTC",
  "login": "TomHarris9",
  "organization": "Honeywell",
  "position": "Sr Manager - Business and Commercial Development",
  "website": "http://www.honeywell-terrastrong.com"
}
```

Lub z poziomu innego skryptu:

```
#!python
from parse import fetch_user_info

downloader = UrlDownloader(600) # request every 10 seconds
info = fetch_user_info(downloader, 'http://www.slideshare.net/TomHarris9')

print 'Full name: %s' % info.full_name
print 'Website: %s' % website
print 'No. followers: %d' % len(info.followers)
```

Po uruchomieniu dostaniemy:

```
Full name: Tom Harris
Websites: http://www.honeywell-terrastrong.com
No. followers: 29
```

### Stworzenie bazy

Do przechowywania danych jest używana jednoplikowa baza danych SQLite o rozszerzeniu **.db3*.


```
#!python

$ python database.py dbname
```


gdzie dbname to opcjonalny parametr nazwy bazy. Domyślna nazwa to *slidshare*. Plik bazy tworzony jest w aktualnym katalogu wywołania.

lub z kodu:



```
#!python

from database import create_database

create_database()
```

### Pobieranie danych

Za pobieranie danych odpowiada moduł *download.py*. Korzysta on z modułów *database.py*, *extract.py* oraz *slideshare.py*. Domyślnie korzysta z bazy *slideshare.db3*.

#### Pobieranie brakujących polecanych prezentacji

```
#!python

$ python download.py s
```

lub z kodu 


```
#!python

from download import download_slideshows_for_related

download_slideshows_for_related()
```

#### Pobieranie prezentacji po jej id

```
#!python

from download import download_slideshow_by_id

download_slideshow_by_id(1234)
```



#### Pobieranie brakujących użytkowników

```
#!python

$ python download.py u
```

lub z kodu 


```
#!python

from download import download_users_for_slideshows

download_users_for_slideshows()
```