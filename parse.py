import re
import json
from pyquery import PyQuery as pq
import datetime
import sys
from downloader import UrlDownloader

# ------------------------------------------------------------------------- utils 


def log(msg):
    print '// %s' % msg

def err(msg):
    print '// %s' % msg

def remove_nl(string):
    return string.replace('\r\n', ' ').replace('\r', ' ').replace('\n', ' ')

def to_json(obj):
    return json.dumps(obj, cls=MyEncoder, sort_keys=True, indent=2, separators=(',', ': '))

def get_text_if_exists(element):
    return element.text() if element else None

def get_meta_property_if_exists(d, property):
    element = d('meta[property="%s"]' % property)
    if element:
        return element.attr('content')
    else:
        None

def extract_login_from_url(url):
    return re.sub('.*slideshare.net/(.*)', r'\1', url.strip()) if url else None

def string_to_time(string):
    if string is None:
        return None 

    def try_parsing(pattern):
        try:
            return datetime.datetime.strptime(string, pattern)
        except:
            return None

    patterns = ['%Y-%m-%dT%H:%M:%SZ', '%Y-%m-%d %H:%M:%S %Z']

    for pattern in patterns:
        date = try_parsing(pattern)
        if date is not None:
            return date

    return datetime.datetime.now()


# ------------------------------------------------------------------------- 

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime("%d-%m-%Y %H:%M:%S")
        if isinstance(obj, (SlideshareInfo, UserInfo, Comment)):
            return obj.__dict__
        #elif isinstance(obj, Comment):
            #return { 
                #'user_login': obj.user_login, 
                #'body': obj.body, 
                #'fetch_date': obj.fetch_date.strftime("%d-%m-%Y %H:%M:%S"), 
                #'created_at': obj.created_at, 
                #'approximate_creation_date': obj.approximate_creation_date.strftime("%d-%m-%Y %H:%M:%S") }
        else:
            return super(MyEncoder, self).default(obj)

class UserInfo:
    def __init__(self, login, full_name, joined, position, organization, industry, country, city, description, website, followers, followings):
        self.login = login
        self.full_name = full_name
        self.joined = joined
        self.position = position
        self.organization = organization
        self.industry = industry
        self.country = country
        self.city = city 
        self.description = description
        self.website = website
        self.followers = followers
        self.followings = followings


class SlideshareInfo:
    def __init__(self, featured_on, updated_at, created_at, download_count, comment_count, 
            embed_count, view_count, author, description, title, comments, likes, category, transcript, related):
        self.featured_on = featured_on
        self.updated_at = updated_at
        self.created_at = created_at
        self.download_count = download_count
        self.comment_count = comment_count
        self.embed_count = embed_count
        self.view_count = view_count
        self.author = author
        self.description = description
        self.title = title
        self.comments = comments
        self.likes = likes
        self.category = category
        self.transcript = transcript
        self.related = related


class Comment:
    def __init__(self, user_login, body, created_at):
        def remove_links_to_users():
            return re.sub("<a[^>]+>(@[^@t]+?)</a>", r'\1', body)

        self.user_login = user_login
        self.body = remove_links_to_users()
        self.created_at = created_at
        self.fetch_date = datetime.datetime.now()
        self.approximate_creation_date = self.__relative_date_to_approximate_absolute_date(self.fetch_date, created_at)

    def __relative_date_to_approximate_absolute_date(self, date_from, date):
        def extract_number(date):
            matcher = re.match('(\d+) \w+', date)
            if matcher:
                return int(matcher.group(1))
            else:
                err('invalid date: ' + date)
                return 0

        if 'second' in date:
            return date_from - datetime.timedelta(seconds = extract_number(date))
        if 'minute' in date:
            return date_from - datetime.timedelta(minutes = extract_number(date))
        if 'hour' in date:
            return date_from - datetime.timedelta(hours = extract_number(date))
        if 'day' in date:
            return date_from - datetime.timedelta(days = extract_number(date))
        if 'weeks' in date:
            return date_from - datetime.timedelta(weeks = extract_number(date))
        if 'month' in date:
            return date_from - datetime.timedelta(days = 30 * extract_number(date))
        if 'year' in date:
            return date_from - datetime.timedelta(days = 365 * extract_number(date))

        err('invalid date: ' + date)
        return date_from


# ------------------------------------------------------------------------- 


def fetch_slideshare_info(downloader, slideshare_direct_link):
    def extract_presentation_id(dom_query_object):
        matcher = re.match('.*"presentationId"\s*:\s*(\d+).*', remove_nl(dom_query_object("script#page-json").text()))

        if not matcher:
            err('presentation id not found, cannot proceed')
            return None

        return int(matcher.group(1))

    def extract_comments(presentation_id):
        data = downloader.download('http://www.slideshare.net/~/slideshow/comments/%d.json' % (presentation_id))
        raw_comments = json.loads(data)
        return map(lambda raw_comment_data: Comment(
            raw_comment_data['login'], 
            raw_comment_data['body'], 
            raw_comment_data['created_at']), raw_comments)

    def extract_likes(presentation_id):
        result = json.loads('[]')

        offset = 0
        while True:
            log('offset %d' % offset)
            data = downloader.download('http://www.slideshare.net/~/slideshow/favorites_list/%d.json?offset=%d' % (presentation_id, offset))
            loaded = json.loads(data, 'latin1')
            if len(loaded) == 0:
                break

            for like in loaded:
                result.append(like)

            offset += 20

        log('number of likes: %d' % len(result))

        return map(lambda user: user['login'], result)

    def extract_related_presentations(dom_query_object):
        related = []
        for link_to_related in dom_query_object('a.j-related-impression'):
            related.append(pq(link_to_related).attr('href'))
        return related


    log('downloading page')
    d = pq(downloader.download(slideshare_direct_link))

    log('extracting presentation id')
    presentation_id = extract_presentation_id(d)

    log('extracting metadata')
    category = get_meta_property_if_exists(d, 'slideshare:category')
    featured_on = string_to_time(get_meta_property_if_exists(d, 'slideshare:featured_on'))
    updated_at = string_to_time(get_meta_property_if_exists(d, 'slideshare:updated_at'))
    created_at = string_to_time(get_meta_property_if_exists(d, 'slideshare:created_at'))
    download_count = get_meta_property_if_exists(d, 'slideshare:download_count')
    comment_count = get_meta_property_if_exists(d, 'slideshare:comment_count')
    embed_count = get_meta_property_if_exists(d, 'slideshare:embed_count')
    view_count = get_meta_property_if_exists(d, 'slideshare:view_count')
    author = extract_login_from_url(get_meta_property_if_exists(d, 'slideshare:author'))
    description = get_meta_property_if_exists(d, 'og:description')
    title = get_meta_property_if_exists(d, 'og:title')

    print view_count

    log('extracting transcript')
    transcript = []
    for li in d('.transcripts > li'):
        transcript.append(re.sub('(\d+\.)\s+', r'\1 ', pq(li).text().strip()))

    log('downloading comments')
    comments = extract_comments(presentation_id)

    log('downloading likes')
    likes = extract_likes(presentation_id)

    log('extracting related presentations')
    related = extract_related_presentations(d)

    return SlideshareInfo(
            featured_on = featured_on,
            updated_at = updated_at,
            created_at = created_at,
            download_count = download_count,
            comment_count = comment_count,
            embed_count = embed_count,
            view_count = view_count,
            author = author,
            description = description,
            title = title,
            comments = comments,
            likes = likes,
            category = category, 
            transcript = transcript, 
            related = related)


# ------------------------------------------------------------------------- 


def fetch_user_info(downloader, user_profile_direct_link):
    d = pq(downloader.download(user_profile_direct_link))

    followers = []
    followings = []

    login = extract_login_from_url(user_profile_direct_link)
    full_name = d('h1[itemprop="name"]').text()
    position = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="jobTitle"]'))
    organization = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="worksFor"]'))
    industry = get_text_if_exists(d('.profileHeader > .profileData > li [title="Industry"] *:not(.dataLabel)'))
    country = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="addressCountry"]'))
    city = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="addressLocality"]'))
    description = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="description"]'))
    website = get_text_if_exists(d('.profileHeader > .profileData > li [itemprop="url"]'))
    joined = pq(d('meta[property="slideshare:joined_on"]')).attr('content')

    # fetch followers 
    log('fetching followers')
    page = 1
    d = pq(downloader.download('http://www.slideshare.net/%s/followers/%d' % (login, page)))
    while not 'No followers yet' in d('.mainContent .panel').text():
        log('page %d' % page)
        for follower_link in d('.mainContent .panel div.userMeta_profile a[rel="contact"]'):
            follower_login = pq(follower_link).attr('href')[1:]
            followers.append(follower_login)

        page += 1
        d = pq(downloader.download('http://www.slideshare.net/%s/followers/%d' % (login, page)))

    # fetch following 
    log('fetching followings')
    page = 1
    d = pq(downloader.download('http://www.slideshare.net/%s/following/%d' % (login, page)))
    while not 'No followings yet' in d('.mainContent .panel').text():
        log('page %d' % page)
        for following_link in d('.mainContent .panel div.userMeta_profile a[rel="contact"]'):
            following_login = pq(following_link).attr('href')[1:]
            followings.append(following_login)

        page += 1
        d = pq(downloader.download(('http://www.slideshare.net/%s/following/%d' % (login, page))))

    return UserInfo(login, full_name, joined, position, organization, industry, country, city, description, website, followers, followings)


# ------------------------------------------------------------------------- 

if __name__ == '__main__':
    downloader = UrlDownloader(1800) # 2 seconds delay
    if len(sys.argv) > 1:
        if sys.argv[1] == 'u':
            for arg in sys.argv[2:]:
                print to_json(fetch_user_info(downloader, arg))
        elif sys.argv[1] == 'p':
            for arg in sys.argv[2:]:
                print to_json(fetch_slideshare_info(downloader, arg))
        else:
            print 'Unknown option %s' % sys.argv[1]
    else:
        print 'Too few arguments'
