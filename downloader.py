import urllib2
import datetime
import time

class UrlDownloader:
    def __init__(self, requests_per_hour):
        self.__time_in_millis_between_requests = datetime.timedelta(milliseconds = 60 * 60 * 1000 / requests_per_hour)
        self.__last_request = datetime.datetime.min

    def download(self, url):
        now = datetime.datetime.now()
        diff = now - self.__last_request

        if self.__time_in_millis_between_requests > diff:
            to_sleep = self.__time_in_millis_between_requests - diff
            seconds_to_sleep = float(to_sleep.microseconds) / 10**6 + to_sleep.total_seconds()
            print "// waiting %.2f seconds before download" % seconds_to_sleep
            time.sleep(seconds_to_sleep)

        print '// downloading'
        return self.__download_internal(url)

    def __download_internal(self, url):
        self.__last_request = datetime.datetime.now()
        headers = { 'User-Agent' : "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0" }
        req = urllib2.Request(url, None, headers)
        return urllib2.urlopen(req).read().decode('utf-8', 'replace')
