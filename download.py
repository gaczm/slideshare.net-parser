# -*- coding: utf-8 -*-
from database import * 
from extract import *
from slideshare import *
from sqlalchemy import exc
from parse import fetch_slideshare_info, fetch_user_info
from downloader import UrlDownloader
import xml.etree.cElementTree as et
import xmltodict

USERNAME_BASE_URL = 'http://www.slideshare.net/'

def get_attributes(self):
    return [attr for attr in dir(self) if not attr.startswith("__") and not attr.startswith("_")]

def save_if_possible(data):
    session = get_session()
    for item in data:
        try:
            session.add(item)
            session.commit()
            session.expunge(item)
        except exc.IntegrityError as e:
            session.rollback()
            print "Unable to save item: {0}".format(e)

def fill_and_save(extracted):
    comments = []
    likes = []
    for slide in extracted.slideshows:
        slidePage = download_slideshow_page(slide.url)
        slide.category_name = slidePage.category
        slide.transcript = ''.join(slidePage.transcript)
        comments = comments + extract_comments_from_page(slide.slideshow_id, slidePage)
        likes = likes + extract_likes_from_page(slide.slideshow_id, slidePage)
    print '// saving {0} slideshows, {1} related slideshows, {2} slideshows tag, {3} comments, {4} likes'.format(
                    len(extracted.slideshows), len(extracted.relates), len(extracted.slideshowTags),
                    len(comments), len(likes))
    save_if_possible(extracted.slideshows)
    save_if_possible(extracted.relates)
    save_if_possible(extracted.slideshowTags)
    save_if_possible(comments)
    save_if_possible(likes)

def download_slideshows(xml_from_request, keyXml):
    slidshow_xml = xmltodict.parse(xml_from_request)
    if int(slidshow_xml[keyXml]['Count']) > 0:
        extracted = extract_slideshows_tags_related(slidshow_xml[keyXml]['Slideshow'])
        fill_and_save(extracted)
    else:
        print '// No slideshows'
    
def download_slideshow_page(url):
    downloader = UrlDownloader(600) 
    return fetch_slideshare_info(downloader, url)

def download_user_page(username):
    downloader = UrlDownloader(600) 
    return fetch_user_info(downloader, USERNAME_BASE_URL + username)

def download_slideshows_by_tag(tag, limit=100):
    print '// downloading slideshows by tag ' + tag
    xml_from_request = get_slideshows_by_tag(tag, limit)
    download_slideshows(xml_from_request, 'Tag')
    
def download_slideshows_by_user(user, limit=100):
    print '// downloading slideshows by user ' + user
    xml_from_request = get_slideshows_by_user(user, limit)
    download_slideshows(xml_from_request, 'User')

def download_slideshow_by_id(id):
    print '// downloading single slideshow with id: ' + str(id)
    xml_from_request = get_slideshow_by_id(id)
    if 'SlideShow Not Found' not in xml_from_request:    
        slidshow_xml = xmltodict.parse(xml_from_request)
        extracted = extract_single_slideshow(slidshow_xml['Slideshow'])
        fill_and_save(extracted)
        return extracted

def download_favourites_for_user(username):
    print '// downloading favourites for user: ' + username
    xml_request = get_user_favorites(username)
    favourite_xml = xmltodict.parse(xml_request)
    favourites = favourite_xml['favorites']  
    if favourites is None:
        return []
    return extract_favourites(username, favourites['favorite'])
    
def download_contacts_for_user(username):
    print '// downloading contacts for user: ' + username  
    xml_request = get_user_contacts(username)
    contacts_xml = xmltodict.parse(xml_request)
    contacts = contacts_xml['Contacts']  
    if contacts is None:
        return []
    return extract_contacts(username, contacts['Contact'])
    

def create_user(user_info, username):
    user = User()
    user.login = username
    user.city = user_info.city
    user.country = user_info.country
    user.full_name = user_info.full_name
    user.join_date = to_datetime(user_info.joined, DATETIME_FORMAT)
    user.organization = user_info.organization
    user.position = user_info.position
    user.website = user_info.website
    return user

def download_user_data(username):
    print '// downloading user data for '+username
    user_info = download_user_page(username)
    user = create_user(user_info, username)
    followers = extract_followers_for(username, user_info.followers)
    followings = extract_followings_for(username, user_info.followings)
    favourites = download_favourites_for_user(username)
    contacts = download_contacts_for_user(username)
    print '// saving {0} user, {1} followers, {2} followings, {3} favourites, {4} contacts'.format(
                    username, len(followers), len(followings),
                    len(favourites), len(contacts))
    save_if_possible([user])
    save_if_possible(followers)
    save_if_possible(followings)
    save_if_possible(favourites)
    save_if_possible(contacts) 

def download_users_for_slideshows():
    users = get_not_existing_users_for_slideshow()
    for user in users:
        try:
            download_user_data(user)
        except Exception as e: 
            print 'Unexpected error: {0} during downloading user {1}'.format(e, user)
                
                
def download_slideshows_with_related(id, limit=10):
    if limit > 0 and not exist(Slideshow.slideshow_id, id):
        extracted = download_slideshow_by_id(id)
        for related in extracted.relates:
            download_slideshows_with_related(related.relating_slideshow_id, limit - 1) 

def download_slideshows_for_related():
    for related in get_not_existing_relating():
        try:
            if not exist(Slideshow.slideshow_id, related.relating_slideshow_id):    
                print '// downloading for relating '+str(related.relating_slideshow_id)
                download_slideshow_by_id(related.relating_slideshow_id)
        except Exception as e:  
             print 'Unexpected error: {0} during downloading related slideshow '.format(e)
                        
def merge_tags():
    session = get_session()
    tags = get_all(SlideshowTag)
    for tag in tags:
        if not exist(Tag.tag_name, tag.tag_name):
            session.add(Tag(tag_name=tag.tag_name))
            session.commit()
            
            
if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == 'u':
            download_users_for_slideshows()
        elif sys.argv[1] == 's':
            download_slideshows_for_related()
        else:
            print 'Unknown option %s' % sys.argv[1]
    else:
        print 'Too few arguments'
