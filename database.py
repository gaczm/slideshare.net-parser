# -*- coding: utf-8 -*-
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.orm import *
import sys

default_categories = ['Art & Photos', 'Automotive', 'Business', 'Career', 'Data & Analytics', 'Design', 'Devices & Hardware',
                      'Economy & Finance', 'Education', 'Engineering', 'Entertainment & Humor', 'Environment', 'Food',
                      'Government & Nonprofit', 'Health & Medicine', 'Healthcare', 'Internet', 'Investor Relations',
                      'Law', 'Leadership & Management', 'Lifestyle', 'Marketing', 'Mobile', 'News & Politics', 'Presentations & Public Speaking',
                      'Real Estate', 'Recruiting & HR', 'Retail', 'Sales', 'Science', 'Self Improvement', 'Services',
                      'Small Business & Entrepreneurship', 'Social Media', 'Software', 'Spiritual', 'Sports', 'Technology', 'Travel']

default_industries = ['Accounting / Auditing', 'Advertising / Marketing / PR', 'Agriculture / Forests / Fisheries', 'Apparel / Fashion', 'Automotive', 'Aviation',
                      'Chemicals', 'CleanTech / Environment', 'Consulting / Advisory', 'Consumer Products', 'Design', 'Education', 'Electronics / Computer Hardware', 'Energy / Oil',
                      'Entertainment / Sports', 'Finance / Banking / Insurance', 'Government / Military', 'Hospitality / Leisure', 'Human Resources / Recruitment', 'Legal',
                      'Logistics / Supply Chain', 'Manufacturing', 'Marine / Shipbuilding', 'Medical / Health Care / Pharmaceuticals', 'Mining / Metals', 'Movies/ Television / Radio',
                      'NGO / Public Service', 'Photography / Arts', 'Presentations / Communications', 'Railways', 'Real Estate/ Construction / Architecture', 'RealEstate',
                      'Religion / Spirituality', 'Retail', 'Technology / Software / Internet', 'Telecom / Mobile', 'Venture Capital / Private Equity', 'Writing / Publishing']
Base = declarative_base()

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S %Z'  

def to_datetime(value, frmt): 
    return datetime.strptime(value, frmt)

class Tag(Base):
    __tablename__ = 'tags'
    tag_name = Column(String, primary_key=True)
    slideshows = relationship('Slideshow', secondary='slideshows_tag')
    
class Format(Base):
    __tablename__ = 'formats'
    format_name = Column(String, primary_key=True,)
    slideshows = relationship('Slideshow', backref="format")
    
class SlideshowsType(Base):
    __tablename__ = 'slideshows_types'
    type_id = Column(Integer, primary_key=True)
    type_name = Column(String, nullable=False)
    slideshows = relationship('Slideshow', backref="slideshow_type")

class Category(Base):
    __tablename__ = 'categories'
    category_name = Column(String, primary_key=True)
    slideshows = relationship('Slideshow', backref="category")
 
class Industry(Base):
    __tablename__ = 'industries'
    industry_name = Column(String, primary_key=True)
    users = relationship('User', backref='industry')
       
class User(Base):
    __tablename__ = 'users'
    login = Column(String, primary_key=True)
    city = Column(String)
    country = Column(String)
    organization = Column(String)
    website = Column(String)
    full_name = Column(String)
    position = Column(String)
    description = Column(String)
    join_date = Column(DateTime)
    industry_name = Column(String, ForeignKey('industries.industry_name'))

    
class Follower(Base):
    __tablename__ = 'followers'
    followed_user_login = Column(String, ForeignKey('users.login'), primary_key=True)
    following_user_login = Column(String, ForeignKey('users.login'), primary_key=True)
    followed_user = relationship('User', backref='following_users', primaryjoin=(User.login == followed_user_login))
    following_user = relationship('User', backref='followers', primaryjoin=(User.login == following_user_login))
                                  
class Slideshow(Base):
    __tablename__ = 'slideshows'
    slideshow_id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    description = Column(String)
    creation_date = Column(DateTime, nullable=False)
    modification_date = Column(DateTime, nullable=False)
    language = Column(String, nullable=False)
    transcript = Column(String)
    url = Column(String, nullable=False)
    number_of_downloads = Column(Integer, nullable=False)
    number_of_views = Column(Integer, nullable=False)
    number_of_slides = Column(Integer, nullable=False)
    format_name = Column(String, ForeignKey('formats.format_name'))
    slideshow_type_id = Column(Integer, ForeignKey('slideshows_types.type_id'))
    owner_login = Column(String, ForeignKey('users.login'))
    category_name = Column(String, ForeignKey('categories.category_name'))
    tags = relationship('Tag', secondary='slideshows_tag')
    
    def __setattr__(self, k, v):
        if k in ['creation_date', 'modification_date']:
            super(Slideshow, self).__setattr__(k, to_datetime(v,DATETIME_FORMAT)) 
        else:
            super(Slideshow, self).__setattr__(k, v)
            
class SlideshowTag(Base):
    __tablename__ = 'slideshows_tag'
    slideshow_id = Column(Integer, ForeignKey('slideshows.slideshow_id'), primary_key=True)
    tag_name = Column(String, ForeignKey('tags.tag_name'), primary_key=True)
 
    
class RelatedSlideshow(Base):
    __tablename__ = 'related_slideshows'
    related_slideshow_id = Column(Integer, ForeignKey('slideshows.slideshow_id'), primary_key=True)
    relating_slideshow_id = Column(Integer, ForeignKey('slideshows.slideshow_id'), primary_key=True)
    related_slideshow = relationship('Slideshow', backref='relating_slideshows', primaryjoin=(Slideshow.slideshow_id == related_slideshow_id))
    relating_slideshow = relationship('Slideshow', backref='related_slideshows', primaryjoin=(Slideshow.slideshow_id == relating_slideshow_id))

class Comment(Base):
    __tablename__ = 'comments'
    comment_id = Column(Integer, primary_key=True, autoincrement=True)
    content = Column(String)
    creation_date = Column(DateTime, nullable=False)
    user_login = Column(String, ForeignKey('users.login'), nullable=False)
    slideshow_id = Column(Integer, ForeignKey('slideshows.slideshow_id'), nullable=False)
    user = relationship('User', backref='comments', primaryjoin=(User.login == user_login))
    slideshow = relationship('Slideshow', backref='comments', primaryjoin=(Slideshow.slideshow_id == slideshow_id))

class Favourite(Base):
    __tablename__ = 'favourites'
    user_login = Column(String, ForeignKey('users.login'), primary_key=True)
    slideshow_id = Column(Integer, ForeignKey('slideshows.slideshow_id'), primary_key=True)
    user = relationship('User', backref='favourite_slideshows', primaryjoin=(User.login == user_login))
    slideshow = relationship('Slideshow', backref='favouriting_users', primaryjoin=(Slideshow.slideshow_id == slideshow_id))


class Contact(Base):
    __tablename__ = 'conctacts'
    contacting_user_login = Column(String, ForeignKey('users.login'), primary_key=True)
    contacting_with_user_login = Column(String, ForeignKey('users.login'), primary_key=True)
    contacting_user = relationship('User', backref='contacting_users', primaryjoin=(User.login == contacting_user_login))
    contacting_with_user = relationship('User', backref='contacting_with_users', primaryjoin=(User.login == contacting_with_user_login))    
                                    
     
def create_slideshow_types():
    return [SlideshowsType(type_id=0, type_name='presentation'), SlideshowsType(type_id=1, type_name='document'),
            SlideshowsType(type_id=2, type_name='portfolio'), SlideshowsType(type_id=3, type_name='video')]

def create_formats():
    return [Format(format_name='ppt'), Format(format_name='pdf'), Format(format_name='pps'), Format(format_name='odp'),
            Format(format_name='doc'), Format(format_name='pot'), Format(format_name='txt'), Format(format_name='rdf')]

def create_categories():
    return [Category(category_name=i) for i in default_categories]

def create_industries():
    return [Industry(industry_name=i) for i in default_industries]

def insert_default_values(session):
    data = create_formats() + create_slideshow_types() + create_industries() + create_categories()
    session.add_all(data)
    session.commit()

def get_engine(dbname='slidshare'):
    return create_engine('sqlite:///' + dbname + '.db3')

def get_session(dbname='slidshare'):
    engine = get_engine(dbname)
    Session = sessionmaker(bind=engine)
    session = Session(expire_on_commit=False)
    return session
    
def create_database(dbname='slidshare'):
    engine = get_engine(dbname)
    session = get_session(dbname)
    Base.metadata.create_all(engine)
    insert_default_values(session)
    
def get_all(object_class):
    return get_session().query(object_class).all();

def get_not_existing_relating():
    return get_session().query(RelatedSlideshow).filter( 
                    ~exists().where(
                                    Slideshow.slideshow_id == RelatedSlideshow.relating_slideshow_id                      
                                   )
                    ).all()

def get_not_existing_users_for_slideshow():
    return [t[0] for t in get_session().query(Slideshow.owner_login).filter( 
                    ~exists().where(
                                    User.login == Slideshow.owner_login                     
                                   )
                    ).all()]
                    
def exist(object_field, field_value):
    return get_session().query(exists().where(object_field==field_value)).scalar()

def get_ordered_favourite_slideshows():
    session = get_session()
    return [t[0] for t in session.query(Favourite.slideshow_id, func.count(Favourite.slideshow_id).label('total')).group_by(Favourite.slideshow_id).order_by("total DESC").all()]

if __name__ == '__main__':
    if len(sys.argv) > 1:
        create_database(sys.argv[1])
    else:
        create_database()
